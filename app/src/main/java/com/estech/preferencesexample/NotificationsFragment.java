package com.estech.preferencesexample;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

public class NotificationsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.notif_preference, rootKey);
    }
}
