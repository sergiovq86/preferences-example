package com.estech.preferencesexample;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreferenceCompat;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private boolean firstTime = true;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);

        SwitchPreferenceCompat switchOscuro = findPreference("oscuro");

        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            EditTextPreference marcaAgua = findPreference("marca_agua");
            marcaAgua.setVisible(true);
            marcaAgua.setSummaryProvider(EditTextPreference.SimpleSummaryProvider.getInstance());
        }

        Preference ayuda = findPreference("help");
        Intent intent = new Intent(getContext(), Ayuda.class);
        ayuda.setIntent(intent);

        /*
        Preference webpagePreference = findPreference("webpage");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.escuelaestech.es"));
        webpagePreference.setIntent(intent);

         */

        Preference toastPreference = findPreference("toast");
        toastPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Toast.makeText(getContext(), "Toast de ejemplo", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        EditTextPreference editTextSize = findPreference("edit_text_size");
        editTextSize.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String valorString = (String) newValue;
                try {
                    int valor = Integer.parseInt(valorString);
                    if (valor >= 10 && valor <= 50) {
                        //Toast.makeText(getContext(), "Valor guardado correctamente", Toast.LENGTH_SHORT).show();
                        return true;
                    } else {
                        Toast.makeText(getContext(), "Valor no almacenado. El valor debe estar entre 10 y 50", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                } catch (NumberFormatException e) {
                    Toast.makeText(getContext(), "Valor no almacenado. El valor debe ser un número", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });


        /*Context context = getPreferenceManager().getContext();
        PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);

        SwitchPreferenceCompat modoOscuroPreference = new SwitchPreferenceCompat(context);
        modoOscuroPreference.setKey("modo_oscuro");
        modoOscuroPreference.setTitle("Habilitar modo oscuro");
        modoOscuroPreference.setSummary("Pulsa aquí para habilitar el modo oscuro.");

        EditTextPreference textSizePreference = new EditTextPreference(context);
        textSizePreference.setKey("edit_text_size");
        textSizePreference.setTitle("Introduce tamaño de texto predeterminado");

        PreferenceCategory visualCategory = new PreferenceCategory(context);
        visualCategory.setKey("notifications_category");
        visualCategory.setTitle("Notifications");
        screen.addPreference(visualCategory);
        visualCategory.addPreference(modoOscuroPreference);
        visualCategory.addPreference(textSizePreference);

        Preference visitanos = new Preference(context);
        visitanos.setTitle("Visítanos");
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.escuelaestech.es"));
        visitanos.setIntent(i);

        screen.addPreference(visitanos);

        setPreferenceScreen(screen);*/
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (firstTime) {
            firstTime = false;
            Toast.makeText(getContext(), "Los ajustes son almacenados automáticamente", Toast.LENGTH_SHORT).show();
        }
        if (key.equals("edit_text_size")) {
            Toast.makeText(getContext(), "Valor de tamaño de texto almacenado", Toast.LENGTH_SHORT).show();
        }
    }

    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

}
